#!/usr/bin/env bash

NEWNAME=$(echo "${TR_TORRENT_NAME}" | sed 's/^\[[^]]*\] //g' | sed 's/ \[[^]]*\]\.mkv$/.mkv/g')

mv "${TR_TORRENT_DIR}/${TR_TORRENT_NAME}" "${TR_TORRENT_DIR}/${NEWNAME}"