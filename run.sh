#!/usr/bin/env bash

TITLES=(
    "subsplease+kimetsu+hashira+geiko"
    "subsplease+nige+jouzu+no+wakagimi"
    "subsplease+oshi+no+ko"
    "subsplease+make+heroine+ga+oosugiru"
    "toonshub+the+fable+hulu"
    "erai+raws+kono+subarashii+3+hevc"
    "subsplease+vtuber+nandaga+haishin+kiri+wasuretara+densetsu+ni+natteta"
    "subsplease+akaneko+ramen"
    "subsplease+tokidoki+bosotto+russia+go+de+dereru+tonari+no+alya-san"
)

mkdir -p ~/.local/torrents
mkdir -p ~/.local/torrents/backup
touch ~/.local/torrents/history.log
touch ~/.local/torrents/to_download.log

for title in "${TITLES[@]}"
do
    RSS_URL="https://nyaa.si/?f=0&c=0_0&q=1080+-batch+${title}&page=rss"

    echo "Processing ${RSS_URL}"

    curl --silent "${RSS_URL}" \
        | grep '<link>' \
        | sed -e 's/\<link\>//' -e 's/\<\/link\>//' \
        | tail -n+2 \
        > ~/.local/torrents/to_download.log

    comm -23 <(sort ~/.local/torrents/to_download.log) ~/.local/torrents/history.log \
        | /opt/homebrew/bin/wget -q -i - -nc -P ~/.local/torrents/

    cat ~/.local/torrents/to_download.log ~/.local/torrents/history.log \
        | sort -u \
        | /opt/homebrew/bin/sponge ~/.local/torrents/history.log
done

ls ~/.local/torrents/*.torrent &>/dev/null || exit

cp ~/.local/torrents/*.torrent ~/.local/torrents/backup
open ~/.local/torrents/*.torrent
